defmodule ExBanking.MoneyTest do
  use ExUnit.Case

  alias ExBanking.Money, as: ExMoney

  test "always positive" do
    assert ExMoney.new(-1) == {:error, :wrong_arguments}
    assert ExMoney.new(-1.198298) == {:error, :wrong_arguments}
    assert ExMoney.new(-0.01) == {:error, :wrong_arguments}
    assert ExMoney.new("-1") == {:error, :wrong_arguments}
    refute ExMoney.new("-0") == {:error, :wrong_arguments}
    refute ExMoney.new("0") == {:error, :wrong_arguments}
  end

  test "format is correct" do
    assert get_value_and_assert(2, 2.00)
    assert get_value_and_assert(2, 2)
    assert get_value_and_assert("2.423", 2.42)
    assert get_value_and_assert(2.1212, 2.12)
    assert get_value_and_assert(0.25, 0.25)
    assert get_value_and_assert("0.25", 0.25)
    assert get_value_and_assert(1.32984, 1.33)
    assert get_value_and_assert(999, 999.0)
    assert get_value_and_assert(999, 999)
    assert get_value_and_assert(999.123456, 999.12)
  end

  defp get_value_and_assert(raw, compare_to) do
    {:ok, money} = raw |> ExMoney.new()
    assert (money |> ExMoney.format()) == compare_to
  end
end
