defmodule ExBankingTest do
  use ExUnit.Case

  @main_account "coingaming"

  setup context do
    cond do
      context[:bypass_create] == true -> :ok

      true ->
        ExBanking.create_user(@main_account)
        :ok
    end
  end

  @tag :bypass_create
  test "create user" do
    assert ExBanking.create_user(@main_account) == :ok
    assert ExBanking.create_user(@main_account) == {:error, :user_already_exists}
    assert ExBanking.create_user("another_user") == :ok
  end

  test "deposit, get_balance and withdraw" do
    assert ExBanking.get_balance(@main_account, "ETH") == {:ok, 0.00}
    assert ExBanking.deposit(@main_account, 12, "ETH") == {:ok, 12.0}
    assert ExBanking.get_balance(@main_account, "ETH") == {:ok, 12.00}
    assert ExBanking.deposit(@main_account, 0.23, "ETH") == {:ok, 12.23}
    assert ExBanking.get_balance(@main_account, "ETH") == {:ok, 12.23}
    assert ExBanking.get_balance(@main_account, "ETH") == {:ok, 12.23}
    assert ExBanking.get_balance(@main_account, "USD") == {:ok, 0.00}
    assert ExBanking.withdraw(@main_account, 0.23, "ETH") == {:ok, 12.00}
    assert ExBanking.withdraw(@main_account, 0.23, "USD") == {:error, :not_enough_money}
    assert ExBanking.withdraw(@main_account, 12, "ETH") == {:ok, 0.00}
  end

  test "send" do
    receiver = "my_friend"
    send = fn -> ExBanking.send(@main_account, receiver, 12, "XRP") end

    assert ExBanking.send("random_account", receiver, 12, "XRP") == {:error, :sender_does_not_exist}
    assert send.() == {:error, :receiver_does_not_exist}
    assert ExBanking.create_user(receiver) == :ok
    assert send.() == {:error, :not_enough_money}
    assert ExBanking.deposit(@main_account, 120.54, "XRP") == {:ok, 120.54}
    assert send.() == {:ok, 108.54, 12.0}
    assert ExBanking.send(@main_account, receiver, 120, "BTC") == {:error, :not_enough_money}
    assert send.() == {:ok, 96.54, 24.0}
  end

  test "can't withdraw when doesn't have balance for given currency" do
    assert ExBanking.withdraw(@main_account, 99.50, "USD") == {:error, :not_enough_money}
    assert ExBanking.deposit(@main_account, 99.49, "USD") == {:ok, 99.49}
    assert ExBanking.deposit(@main_account, 100, "ETH") == {:ok, 100}
    assert ExBanking.withdraw(@main_account, 99.50, "USD") == {:error, :not_enough_money}
  end

  test "can't get balance for inexistent user" do
    assert ExBanking.get_balance("alfred", "USD") == {:error, :user_does_not_exist}
  end

  test "can't create duplicated user" do
    assert ExBanking.create_user(@main_account) == {:error, :user_already_exists}
  end
end
