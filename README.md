# ExBanking

Implementation of Coingaming Elixir's test available at [https://github.com/coingaming/elixir-test](https://github.com/coingaming/elixir-test).

## Introduction

Due to the test's [Performance](https://github.com/coingaming/elixir-test#performance) requirements I considered 3 approaches:

1. All of the users' accounts in a single ETS table (a single and global Bank Vault). Keys would be the `user` name and the value, a `Map` where each key would be a `currency` which the user has balance. Backpressure would be implemented as another ETS table, with `update_counter` as each operation is completed/added.
2. A `GenStage` pipeline, with *automagically* provided backpressure.
3. Separate user Wallets as individual `GenServer`'s (each user balance and currencies kept isolated in the user's process) and a simple backpressure machanism implemented by the `GenServer` native `:message_queue_len`.

The **1st** option is available in the repository's tag `single-ets-table-wallet`, but it is imcomplete, without backpressure. The final implementation follows the **3rd option**, which is the code in the `master` branch.

Why not going with `GenStage`? `GenStage` made it very easy to implement all of the specs, specially backpressure, but when implementing the **send** operation, there was no straightforward way to implement separate mechanisms to decided whether there were `:too_many_requests_to_sender` or `:too_many_requests_to_receiver`, I would have to always throw `:too_many_requests_to_user` (which would come by default from my `GenStage` Producer's demand setup). It would make things too complicated for the purpose of this test, so I preferred to go with **3**.

## How It Works

- API is exposed by the [ExBanking](lib/ex_banking.ex) module, as per specs.
- `ExBanking` creates and defines an [Operation](lib/ex_banking/operation.ex) and then submit it to the Bank [Vault](lib/ex_banking/bank/vault.ex).
- The Vault takes care of creating and coordinating user accounts ([Wallet](lib/ex_banking/bank/user/wallet.ex)).
- User accounts are tracked by Elixir's `Registry`.
- A Wallet is a `GenServer` which keeps the user balance and each provided currency and also performs the operations (as `call`).
- A `send` operation is performed as separate steps, using both the `sender` and the `receiver` wallets, taking advantage of their isolation and separate backpressure treatment:

  1. Check whether both the `sender` and `receiver` exists, otherwise returning either `    :sender_does_not_exist | :receiver_does_not_exist`.
  2. Check whether both can receive additional operations, otherwise returning either `:too_many_requests_to_sender | :too_many_requests_to_receiver`.
  2. Perform the default `withdraw` operation from the `sender` (which involves checking whether there is enough balance for the currency).
  3. If all good, perform the default `deposit` operation into the `receiver`.  

## Tests

Some tests were implemented. Run with `mix test`.

## Final Considerations

- In a real-life scenario depending on the whether there would be a data store / central coordinator, everything could be changed drastically. Since right now also all users are isolate and there is no central vault/wallet.
- Issue: In a `send` operation, in case of crash after the withdraw from the `sender`, there is no guarantee that the `receiver` will receive the deposit. I don't think it is in the scope of the test to implement such guarantee.
- If isolate wallets are the final idea, it may be a good idea to perform the operations in the Vault (since Vault is the coordinator, the "bank" itself), and keep just the data itself in a Wallet.

## Author

Alfred R. Baudisch
[alfred.r.baudisch@gmail.com](mailto:alfred.r.baudisch@gmail.com)
