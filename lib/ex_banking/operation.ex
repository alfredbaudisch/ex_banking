defmodule ExBanking.Operation do
  @moduledoc """
  Describes a bank operation. Deal with all current
  implemented operations.
  """

  @id_length 16

  @type operation_type ::
    :deposit      |
    :withdraw     |
    :get_balance  |
    :send

  @type t :: %ExBanking.Operation{
    id: String.t,
    type: operation_type,
    user: String.t,
    receiver: String.t,
    money: Money.t,
    currency: String.t
  }
  defstruct [
    :id,
    :type,
    :user,
    :receiver,
    :money,
    :currency
  ]

  def make_deposit(user, amount, currency), do:
    make(:deposit, user, currency, amount)

  def make_withdraw(user, amount, currency), do:
    make(:withdraw, user, currency, amount)

  def make_get_balance(user, currency), do:
    make(:get_balance, user, currency)

  def make_send(from_user, to_user, amount, currency) do
    case make(:send, from_user, currency, amount) do
      {:ok, base} -> {:ok, %{base | receiver: to_user}}
      error -> error
    end
  end

  defp make(type, user, currency, amount \\ 0)
  defp make(type, user, currency, amount)
  when is_binary(user) and is_binary(currency) and is_number(amount) and amount >= 0 do
    case ExBanking.Money.new(amount) do
      {:ok, money} -> make(type, user, currency, money)
      error -> error
    end
  end
  defp make(type, user, currency, %Money{} = money) do
    {:ok,
      %__MODULE__{
        id: SecureRandom.hex(@id_length), # Simulate a unique ID per transaction
        type: type,
        user: user,
        currency: currency,
        money: money
      }
    }
  end
  defp make(_, _, _, _), do: {:error, :wrong_arguments}
end
