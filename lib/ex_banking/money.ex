defmodule ExBanking.Money do
  @moduledoc """
  Provides currency treatment to comply with format requirements.
  Main logic provided by the package liuggio/money.
  """

  # The library "Money" requires a "real" currency to be set, so consider everything as USD
  @simulate_currency :USD

  def new_raw(amount), do: {:ok, Money.new(amount, @simulate_currency)}

  def new(amount) when is_integer(amount), do:
    amount |> to_string() |> new()

  def new(amount) do
    case Money.parse(amount, @simulate_currency) do
      {:ok, money} ->
        if Money.negative?(money), do: {:error, :wrong_arguments}, else: {:ok, money}

      _ -> {:error, :wrong_arguments}
    end
  end

  def add(money, add), do: Money.add(money, add)

  def subtract(money, subtract), do: Money.subtract(money, subtract)

  def compare(left, right), do: Money.compare(left, right)

  def format(money) do
    {formatted, _} =
      money
      |> Money.to_string(symbol: false, separator: "", delimiter: ".")
      |> Float.parse()
    formatted
  end

  def get_value(%Money{amount: amount}), do: amount
end
