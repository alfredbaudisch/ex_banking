defmodule ExBanking.Application do
  @moduledoc """
  Main supervisor.
  """

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: true

    children = [
      supervisor(Registry, [:unique, ExBanking.Bank.Vault.users_registry_name()]),
      ExBanking.Bank.Vault
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: ExBanking.Supervisor)
  end
end
