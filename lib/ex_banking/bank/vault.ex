defmodule ExBanking.Bank.Vault do
  @moduledoc """
  Coordinates User wallets (bank accounts) and Operations.
  """

  use Supervisor

  @registry ExBanking.Bank.User.Registry
  @max_requests_per_user 10

  alias ExBanking.{Operation, Bank.User.Wallet}

  def start_link(_arg) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_opts) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def create_user(user) do
    case get_user(user) do
      [{_, pid}] when not is_nil(pid) ->
        {:error, :user_already_exists}

      _ ->
        {:ok, pid} = DynamicSupervisor.start_child(__MODULE__, %{
          id: Wallet,
          start: {Wallet, :start_link, [user]}
        })

        Registry.register(@registry, user, pid)

        :ok
    end
  end

  @doc """
  Deals with finding an user, checking if the user can perform another operation
  (backpressure) and then submit the operation to the user wallet (to the user pid).

  The `send` operation is performed by `send_operation/2`, which first performs
  a withdraw from the sender, then next perform a deposit into the receiver,
  as two separate operations.

  ISSUE: If there is a crash right after the withdraw and before the deposit,
  the receiver won't receive the money. But this is an initial idea/implementation
  which covers the essence of the requirements.
  """
  def operation(%Operation{user: user} = details), do:
    get_user(user) |> operation(details)
  def operation([{_, pid}], %{type: :send} = details), do: send_operation(pid, details)
  def operation([{_, pid}], details), do: operation(pid, details)
  def operation([], %{type: :send}), do: {:error, :sender_does_not_exist}
  def operation([], _), do: {:error, :user_does_not_exist}
  def operation(pid, details) when is_pid(pid) do
    Process.info(pid, :message_queue_len)
    |> perform_operation(pid, details)
  end

  defp perform_operation({_, length}, _, _) when length > 10, do:
    {:error, :too_many_requests_to_user}
  defp perform_operation(_, pid, details), do:
    Wallet.perform_operation(pid, details)

  defp can_perform_operation?(pid) do
    case Process.info(pid, :message_queue_len) do
      {_, length} when length >= @max_requests_per_user -> false
      _ -> true
    end
  end

  defp can_perform_send_operation?(sender_pid, receiver_pid) do
    if can_perform_operation?(sender_pid) do
      can_perform_operation?(receiver_pid) || {:error, :too_many_requests_to_receiver}
    else
      {:error, :too_many_requests_to_sender}
    end
  end

  defp send_operation(sender_pid, %Operation{type: :send, receiver: receiver} = details) do
    case get_user(receiver) do
      [{_, receiver_pid}] ->
        # Check whether both sender and receiver can make an additional operation
        with true <- can_perform_send_operation?(sender_pid, receiver_pid) do
          {:ok, withdraw} = Operation.make_withdraw(details.user, details.money, details.currency)

          Wallet.perform_operation(sender_pid, withdraw)
          |> send_operation(receiver_pid, details)

        else
          error -> error
        end

      _ -> {:error, :receiver_does_not_exist}
    end
  end

  defp send_operation({:ok, sender_balance}, receiver_pid, details) do
    {:ok, deposit} = Operation.make_deposit(details.receiver, details.money, details.currency)

    case Wallet.perform_operation(receiver_pid, deposit) do
      {:ok, receiver_balance} -> {:ok, sender_balance, receiver_balance}
      error -> error
    end
  end
  defp send_operation({:error, _} = error, _, _), do: error

  defp get_user(user), do:
    Registry.lookup(users_registry_name(), user)

  def users_registry_name, do: @registry
end
