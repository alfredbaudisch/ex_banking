defmodule ExBanking.Bank.User.Wallet do
  @moduledoc """
  Isolate and specific user bank account and where operations are performed.
  """

  use GenServer

  alias ExBanking.Operation
  alias ExBanking.Money, as: ExMoney

  def start_link(user) do
    GenServer.start_link(__MODULE__, user, id: make_ref())
  end

  def init(_) do
    {:ok, %{}}
  end

  #
  # Client
  #

  def perform_operation(pid, %Operation{} = operation) do
    GenServer.call(pid, {:operation, operation})
  end

  #
  # Server
  #

  def handle_call({:operation, %Operation{type: :get_balance,
  currency: currency}}, _, state) do
    get_currency_and_perform(currency, state, fn balance ->
      {:ok, balance |> ExMoney.format()}
    end)
  end

  def handle_call({:operation, %Operation{type: :deposit,
  money: money, currency: currency}}, _, state) do
    get_currency_and_perform(currency, state, fn balance ->
      new_balance = ExMoney.add(balance, money)
      {
        {:ok, new_balance |> ExMoney.format()},
        :state, put_in(state[currency], new_balance |> ExMoney.get_value())
      }
    end)
  end

  def handle_call({:operation, %Operation{type: :withdraw, money: money,
  currency: currency}}, _, state) do
    get_currency_and_perform(currency, state, fn balance ->
      # Check if the user has enough money to withdraw
      if ExMoney.compare(balance, money) != -1 do
        new_balance = ExMoney.subtract(balance, money)
        {
          {:ok, new_balance |> ExMoney.format()},
          :state, put_in(state[currency], new_balance |> ExMoney.get_value())
        }
      else
        {:error, :not_enough_money}
      end
    end)
  end

  #
  # Common Operations
  #

  defp get_currency_and_perform(currency, current_state, perform) do
    {:ok, money} = do_get_currency(currency, current_state)
    perform.(money) |> final_result(current_state)
  end

  defp final_result({result, :state, new_state}, _), do: reply(result, new_state)
  defp final_result(result, state), do: reply(result, state)

  defp do_get_currency(currency, state) do
    Map.get(state, currency, 0) |> ExMoney.new_raw()
  end

  #
  # Helpers
  #

  defp reply(result, state), do: {:reply, result, state}
end
