defmodule ExBanking do
  @moduledoc """
  The bank public interface, as per Coingaming specs. Operations
  are performed by providing a ExBanking.Operation struct to
  ExBanking.Bank.Vault.
  """

  alias ExBanking.{Operation, Bank.Vault}

  @type banking_error :: {:error,
    :wrong_arguments                |
    :user_already_exists            |
    :user_does_not_exist            |
    :not_enough_money               |
    :sender_does_not_exist          |
    :receiver_does_not_exist        |
    :too_many_requests_to_user      |
    :too_many_requests_to_sender    |
    :too_many_requests_to_receiver
  }

  @spec create_user(user :: String.t) :: :ok | banking_error
  def create_user(user) when is_binary(user), do: Vault.create_user(user)
  def create_user(_), do: {:error, :wrong_arguments}

  @spec deposit(user :: String.t, amount :: number, currency :: String.t) :: {:ok, new_balance :: number} | banking_error
  def deposit(user, amount, currency) do
    Operation.make_deposit(user, amount, currency) |> perform_operation()
  end

  @spec withdraw(user :: String.t, amount :: number, currency :: String.t) :: {:ok, new_balance :: number} | banking_error
  def withdraw(user, amount, currency) do
    Operation.make_withdraw(user, amount, currency) |> perform_operation()
  end

  @spec get_balance(user :: String.t, currency :: String.t) :: {:ok, balance :: number} | banking_error
  def get_balance(user, currency) do
    Operation.make_get_balance(user, currency) |> perform_operation()
  end

  @spec send(from_user :: String.t, to_user :: String.t, amount :: number, currency :: String.t) :: {:ok, from_user_balance :: number, to_user_balance :: number} | banking_error
  def send(from_user, to_user, amount, currency) do
    Operation.make_send(from_user, to_user, amount, currency) |> perform_operation()
  end

  defp perform_operation(operation) do
    case operation do
      {:ok, operation} -> Vault.operation(operation)
      error -> error
    end
  end
end
